

export const validEmail = (email) => {
    let isValid = false;
    let error = '';
    const emailRegex = /^\S+@\S+\.\S+$/;
    if(email.match(emailRegex)){
        isValid = true;
        error = '';
    }
    else{
        isValid = false;
        error = 'Please enter valid email.'
    }

    return{
        isValid,
        error
    }
}

export const validPassword = (password) => {
    let isValid = false;
    let error = '';
    
    if(password.length !== 0){
        isValid = true;
        error = '';
    }
    else{
        isValid = false;
        error = 'Please enter password.'
    }

    return{
        isValid,
        error
    }
}