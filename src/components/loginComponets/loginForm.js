import React, { useState } from "react";
import classes from "../../assets/css/loginForm.module.css";
import { validEmail, validPassword } from "../../utils/validation";
import Users from "../../assets/json/users.json";
import { useHistory } from "react-router-dom/cjs/react-router-dom.min";
import CryptoJS from "crypto-js";



const LoginForm = () => {
  const [email, setEmail] = useState(null);
  const [password, setPassword] = useState(null);
  const [isEmailValid, setIsEmailValid] = useState(false);
  const [isPasswordValid, setIsPasswordValid] = useState(false);
  const [emailError, setEmailError] = useState("");
  const [passwordError, setPasswordError] = useState("");
  const [users] = useState(Users.users);
  const history = useHistory();

  const formSubmitHandler = (event) => {
    event.preventDefault();

    if (isEmailValid && isPasswordValid) {
      let authenticatePerson = users.filter((user) => {
        return user.email === email && user.password === password;
      });

      if (authenticatePerson.length !== 0) {
        
        let cipherPassword = CryptoJS.AES.encrypt(JSON.stringify(password), 'my-secret-key@123').toString();
        localStorage.setItem('userEmail',email);
        localStorage.setItem('password',cipherPassword);
        localStorage.setItem('user_id',authenticatePerson[0].user_id);
        history.replace('/profile');

      } else {
        alert("Not Authenticated");
      }
    } else {
      alert("Please enter valid inputs");
    }
  };

  const onChangeEmailHandler = (event) => {
    setEmail(event.target.value);
    let validation = validEmail(event.target.value);
    setIsEmailValid(validation.isValid);
    setEmailError(validation.error);
  };

  const onChangePasswordHandler = (event) => {
    setPassword(event.target.value);
    let validation = validPassword(event.target.value);
    setIsPasswordValid(validation.isValid);
    setPasswordError(validation.error);
  };

  return (
    <form onSubmit={formSubmitHandler}>
      <div className={classes.container}>
        <label>
          <b>Email</b>
        </label>
        <input
          onChange={onChangeEmailHandler}
          type="email"
          placeholder="Enter Email"
          name="email"
          required
        />
        <p className={classes.danger}>{emailError}</p>
        <label>
          <b>Password</b>
        </label>
        <input
          onChange={onChangePasswordHandler}
          type="password"
          placeholder="Enter Password"
          name="password"
          required
        />
        <p className={classes.danger}>{passwordError}</p>
        <button type="submit">Login</button>
      </div>
    </form>
  );
};

export default LoginForm;
