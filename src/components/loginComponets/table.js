import React, { useEffect, useState } from "react";
import classes from "../../assets/css/table.module.css";
import Invitations from "../../assets/json/invitations.json";
import InvitationsUpdate from "../../assets/json/invitations_update.json";

const Table = (props) => {
  const [currentUserData, setCurrentUserData] = useState(
    Invitations.invites.filter((element) => {
      return element.user_id === props.userId[0];
    })
  );

  const [updatedData, setUpdatedData] = useState(
    InvitationsUpdate.invites.filter((element) => {
      return element.user_id === props.userId[0];
    })
  );



  let tableData = currentUserData.map((element) => {
    return (
      <tr
        className={element.status === "read" ? classes.read : classes.unread}
        key={element.invite_id}
      >
        <td>{element.invite_id}</td>
        <td>{element.sender_id}</td>
        <td>{element.vector}</td>
        <td>{element.status}</td>
      </tr>
    );
  });

  useEffect(() => {
     if (updatedData.length !== 0) {
       let interval = setInterval(() => {
        if(updatedData.length){
          let newData = updatedData;
          let data = newData.shift();
          setCurrentUserData([...currentUserData,data])
          setUpdatedData(newData)
        }
        return clearInterval(interval)
       }, 5000);
     }
  },[updatedData,currentUserData]);

  return (
    <table className={classes.table}>
      <tbody>
        <tr>
          <th>invite_id</th>
          <th>Sender Name</th>
          <th>vector</th>
          <th>status</th>
        </tr>
        {tableData}
      </tbody>
    </table>
  );
};

export default Table;
