import React, { useState } from "react";
import { useHistory } from "react-router-dom/cjs/react-router-dom.min";
import classes from "../../assets/css/profile.module.css";

import Table from "./table";

const Profile = () => {
  const history = useHistory();
  const userId = useState(localStorage.getItem("user_id"));
 

  const onLogoutHandler = () => {
    localStorage.removeItem("userEmail");
    localStorage.removeItem("password");
    localStorage.removeItem("user_id");
    history.replace("/login");
  };

  return (
    <div className={classes.container}>
      <button onClick={onLogoutHandler} type="button">
        Logout
      </button>
      <div>
          <Table userId={userId}/>
      </div>
    </div>
  );
};

export default Profile;
