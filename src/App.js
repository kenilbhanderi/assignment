import LoginForm from "./components/loginComponets/loginForm";
import { Switch, Route, Redirect } from "react-router-dom";
import Profile from "./components/loginComponets/profile";


const App = () => {
  
  const isLogin = localStorage.getItem('userEmail')

  return (
    <div className="App">
      <Switch>
        <Route path="/" exact>
          <Redirect to="/login" />
        </Route>
        <Route path="/login" exact>
          <LoginForm />
        </Route>
        <Route path="/profile" exact>
          <Profile /> 
        </Route> 
        <Route path="*" exact>
          {isLogin !== null? <Redirect to="/profile" /> : <Redirect to="/login" />}
        </Route>
      </Switch>
    </div>
  );
};

export default App;
